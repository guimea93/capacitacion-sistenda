
@extends('layouts.app')

@php
use Carbon\Carbon;
@endphp

@section('contenido')
        <div class="container-fluid">
            <div class="container">
                <div class="row text-center" style="text-align:center">
                    <div class="col-sm-12 text-center">
                        <h3>Restaurante<br>
                    <p><a href="/prueba">Ir a prueba</a></p></h3>
                    </div>
                </div>

                <div class="row">
                    @foreach($usuarios as $usuario)
                        <div class="col-sm-3">
                            <div class="card">
                                <img class="img-fluid" src="{{$usuario->imagen}}">
                                <h5>{{$usuario->name}}</h5>
                                <h5>{{$usuario->email}}</h5>
                                <h5>{{$usuario->telefono}}</h5>
                                <h5>{{$usuario->created_at}}
                                    @php
                                    $fecha=Carbon::parse($usuario->created_at)->format('l, d M Y');
                                    @endphp
                                    <br>
                                    {{$fecha}}
                                </h5>
                            </div>
                        </div>

                    @endforeach
                    <div class="col-sm-12">
                        {{$usuarios->links()}}
                    </div>

                </div>




            </div>

        </div>
        
@endsection
