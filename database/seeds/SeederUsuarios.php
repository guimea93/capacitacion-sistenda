<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class SeederUsuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i=0;$i<100;$i++)
        {
        $faker = Faker::create();
        $usuario = new User;
        $usuario->name = $faker->name;
        $usuario->email=$faker->email;
        $usuario->telefono=$faker->tollFreePhoneNumber;
        $usuario->imagen='/persona.jpg';
        $usuario->password=bcrypt('password');
        $usuario->save();
    	}

    }
}
