<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function prueba()
    {
    	$prueba="Esto es una variable de prueba";
    	return view('prueba')->with('prueba',$prueba);
    }

    public function index()
    {
    	$usuarios=User::orderBy('created_at','ASC')->paginate(8);
    	/*$prueba="Esto es una variable de prueba";
    	dd($usuarios,$prueba);*/
    	return view('welcome')->with('usuarios',$usuarios);
    }
}
